# Python Introduction to NumPy and Pandas

Student files for a course run by the IT Learning Centre @ University of Oxford

&copy; Graham Addis, University of Oxford, released under
[CC BY-NC-SA 4.0]("http://creativecommons.org/licenses/by-nc-sa/4.0/")
licence.

# Try it out online
Launch in [binder](https://mybinder.org) using the following link:

https://mybinder.org/v2/gl/it-learning-centre%2Fcourses%2FPG022_Files_Python_NumPyAndPandasIntroduction/HEAD

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/it-learning-centre%2Fcourses%2FPG022_Files_Python_NumPyAndPandasIntroduction/HEAD)

# Licence

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>